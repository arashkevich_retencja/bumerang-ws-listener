import { Component, OnInit } from '@angular/core';
import Echo from 'laravel-echo';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  weather$: Observable<any>;
  parameters$: Observable<any>;
  global$: Observable<any>;
  ai$: Observable<any>;
  events$: Observable<any>;
  token: string;

  newListeners$: {listener: Observable<any>, objectId: string}[] = [];

  objectId: string;
  listeners: {objectId: string, data: any}[] = [];

  weather = [];
  events = [];
  ai = [];
  parameters = [];
  global = [];
  io = io;
  echo: Echo;

  constructor(private auth: AuthService) {}

  ngOnInit(): void {
    this.auth.getToken().subscribe(response => {
      this.token = response.data.token;
      this.startListener();
    });
  }

  startListener() {
    this.echo = new Echo({
      broadcaster: 'socket.io',
      host: window.location.hostname + ':6001',
      client: this.io,
      auth: { headers: { Authorization: 'Bearer ' + this.token } },
    });

    this.weather$ = Observable.create(observer =>
      this.echo
        .channel('private-bumerang.weather.1')
        .listen('BumerangWeatherTube', weather =>
          observer.next(weather[0]),
        ),
    );

    this.parameters$ = Observable.create(observer =>
      this.echo
        .channel('private-bumerang.parameters.1')
        .listen('BumerangParametersTube', parameters =>
          observer.next(parameters),
        ),
    );

    this.global$ = Observable.create(observer =>
      this.echo
        .channel('private-bumerang.global.1')
        .listen('BumerangGlobalTube', global =>
          observer.next(global),
        ),
    );

    this.ai$ = Observable.create(observer =>
      this.echo
        .channel('private-bumerang.ai.1')
        .listen('BumerangAiTube', ai =>
          observer.next(ai),
        ),
    );

    this.events$ = Observable.create(observer =>
      this.echo
        .channel('private-bumerang.events.1')
        .listen('BumerangEventsTubePush', events =>
          observer.next(events),
        ),
    );

    this.parameters$.subscribe(res => this.parameters = [res, ...this.parameters]);
    this.global$.subscribe(res => this.global = [res, ...this.global]);
    this.weather$.subscribe(res => this.weather = [res, ...this.weather]);
    this.events$.subscribe(res => this.events = [res, ...this.events]);
    this.ai$.subscribe(res => this.ai = [res, ...this.ai]);
  }

  clear() {
    this.global = [];
    this.weather = [];
    this.parameters = [];
    this.events = [];
    this.ai = [];
    this.listeners = [];
  }

  addNewListener() {
    this.newListeners$.push({
      objectId: this.objectId,
      listener: Observable.create(observer => this.echo.channel(`private-bumerang.parameters.1.${this.objectId}`).listen('BumerangParametersTube', parameters => observer.next(parameters))),
    });
    this.listenNewListeners();
  }


  listenNewListeners() {
    this.newListeners$.forEach(listener => listener.listener.subscribe(res =>
      this.listeners = [{objectId: listener.objectId, data: res}, ...this.listeners]));
    this.objectId = '';
  }

}
