import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import FD from 'form-data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(public http: HttpClient) {}

  getToken(): Observable<any> {
    const data = new FD();
    data.append('email', 'maciej.skorczewski@retencja.pl');
    data.append('password', 'test');

    return this.http.post('http://localhost:8080/api/login', data);
  }
}
